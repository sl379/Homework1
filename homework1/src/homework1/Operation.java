package homework1;
/*
 * Operation class
 */
public class Operation {

	protected double num1 = 0;
	protected double num2 = 0;
	protected char op;
	public double getResult()
	{
		double result = 0;
		return result;
	}
	
	Operation(double num1, double num2,char op)
	{
		this.num1 = num1;
		this.num2 = num2;
		this.op = op;
	}
	
	
}

//op +
class OpAdd extends Operation{
	


	OpAdd(double num1, double num2, char op) 
	{
		super(num1, num2, op);
		// TODO Auto-generated constructor stub
	}

	public double getResult()
	{
		double result = 0;
		result = num1 + num2;
		return result;
	}	
}
// op -
class OpSub extends Operation{
	

	OpSub(double num1, double num2, char op) 
	{
		super(num1, num2, op);
		// TODO Auto-generated constructor stub
	}

	public double getResult()
	{
		double result = 0;
		result = num1 - num2;
		return result;
	}	
}
// op *
class OpMul extends Operation{
	


	OpMul(double num1, double num2, char op) 
	{
		super(num1, num2, op);
		// TODO Auto-generated constructor stub
	}

	public double getResult()
	{
		double result = 0;
		result = num1 * num2;
		return result;
	}	
}
// op /
class OpDiv extends Operation{
	


	OpDiv(double num1, double num2, char op) 
	{
		super(num1, num2, op);
		// TODO Auto-generated constructor stub
	}

	public double getResult()
	{
		double result = 0;
		if(num2 == 0)
			throw new ArithmeticException("Wrong input");
		result = num1 / num2;
		return result;
	}
	
}


