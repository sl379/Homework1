package homework1;
/*
 * OperationFactory class
 * 
 */
public class OperationFactory {
	
	public Operation getOp(double num1, double num2, char oper)
	{
		Operation result = null;
		switch(oper)
		{
		case '+':
			result = new OpAdd(num1,num2,oper);
			break;
		case '-':
			result = new OpSub(num1, num2,oper);
			break;
		case '*':
			result = new OpMul(num1, num2,oper);
			break;
		case '/':
			result = new OpDiv(num1, num2, oper);
			break;
		}
		return result;
	}
}
